import * as Buttons from './button';
import * as Colors from './color';
import * as Spacing from './spacing';
import * as Typography from './typography';

export {Typography, Spacing, Colors, Buttons};
