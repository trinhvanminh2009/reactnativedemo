import * as Colors from './color';
import * as Spacing from './spacing';
import * as Typography from './typography';

export const base = {
  alignItems: 'center',
  marginRight: Spacing.smallest,
  marginVertical: Spacing.tiny,
};

export const text = {
  color: Colors.white,
  fontSize: Typography.smallestFontSize,
  fontWeight: 'bold',
  letterSpacing: Spacing.supperTiny,
};

export const textUnselected = {
  ...text,
  color: Colors.mediumGray,
};

export const small = {
  paddingHorizontal: Spacing.small,
  paddingVertical: Spacing.base,
  width: Spacing.buttonWidth,
};

export const large = {
  paddingHorizontal: Spacing.large,
  paddingVertical: Spacing.largest,
};

export const rounded = {
  borderRadius: Spacing.circleRadius,
};

export const selected = {
  backgroundColor: Colors.selected,
};

export const unselected = {
  backgroundColor: Colors.unselected,
};

export const smallRounded = {
  ...base,
  ...small,
  ...rounded,
};
