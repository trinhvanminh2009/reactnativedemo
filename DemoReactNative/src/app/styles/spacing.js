export const supperTiny = 1;
export const tiny = 3;
export const smallest = 4;
export const smaller = 8;
export const small = 12;
export const base = 16;
export const large = 20;
export const larger = 24;
export const largest = 28;
export const extraLarge = 50;

export const buttonWidth = 100;
export const buttonHeight  = 50;
export const circleRadius = 50;
export const baseBorderRadius = 10;
export const smallViewHeight = 20;

export const imageWidthLarge = 70;
export const imageHeightLarge = 70;
