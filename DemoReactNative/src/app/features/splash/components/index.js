import React, { Component } from "react";
import {StyleSheet, View, Image} from 'react-native';
import styles from './styles'
export default class Splash extends Components{
    render(){
        return (
            <View style = {styles.container}>
                <Image style = {styles.logo}
                    source = {{uri: 'data:../../../../../assets/logo_large.png'}}
                />
            </View>
        )
    }
}