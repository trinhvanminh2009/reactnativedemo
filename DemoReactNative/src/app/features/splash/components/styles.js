import {StyleSheet} from 'react-native';
import {background} from '../../../styles/color';
import * as spacing from '../../../styles/spacing';

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: background,
  },
  logo:{
    width: spacing.imageWidthLarge,
    height: spacing.imageHeightLarge,
  }
});
