import {StackRouter} from '@react-navigation/native';
import React from 'react';
import * as screenNames from '../screenName';
import Slash  from '../../features/splash/containers';

export const appNavigator = StackRouter({
    [screenNames.SPLASH]:{
        screen: Slash
    },
})