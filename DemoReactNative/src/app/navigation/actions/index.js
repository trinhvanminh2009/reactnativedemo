import RNExitApp from 'react-native-exit-app';
import * as screenNames from '../screenName';
import {CommonActions} from '@react-navigation/native';
export const navigateBack = () => CommonActions.goBack();

export const navigateToSplash = () =>
  CommonActions.navigate({
    routeName: screenNames.SPLASH,
  });

export const navigateToLogin = () =>
  CommonActions.navigate({
    routeName: screenNames.LOGIN,
  });

export const navigateToHome = () =>
  CommonActions.navigate({
    routeName: screenNames.HOME,
  });

export const exitApp = () => RNExitApp.exitApp();
